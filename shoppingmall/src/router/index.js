import Vue from "vue";
import VueRouter from "vue-router";
import home from "../views/home.vue";
import productDetail from "../views/productDetail.vue";
import contact from "../views/contact.vue";

Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "home",
        component: home,
    },
    {
        path: "/product-detail",
        name: "product-detail",
        component: productDetail,
        meta: {
            layout: "layoutProductDisplay",
        },
    },
    {
        path: "/contact",
        name: "contact",
        component: contact,
        meta: {
            layout: "layoutContact",
        },
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;