import Vue from "vue";
import Vuex from "vuex";
import * as products from "./modules/products.js";
import * as productDetail from "./modules/productDetail.js";
import * as review from "./modules/reviewForm.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    Cart: 0
  },
  modules: {
    products,
    productDetail,
    review
  },
  mutations: {
    SELECT_QUANTITYPRODUCTTOBUY(state) {
      state.Cart++;
    }
  },
  actions: {
    selectQuantityProductToBuy({ commit }) {
      commit("SELECT_QUANTITYPRODUCTTOBUY");
    }
  }
});
