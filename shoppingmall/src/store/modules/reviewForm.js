export const state = {
  review: {}
};
export const mutations = {
  REVIEW_OFGUEST(state, review) {
    state.review = review;
  }
};
export const actions = {
  reviewOfGuest({ commit }, review) {
    commit("REVIEW_OFGUEST", review);
  }
};
export const getters = {};
