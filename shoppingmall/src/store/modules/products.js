import axios from "axios";

export const state = {
  products: []
};

export const mutations = {
  LOAD_PRODUCTS(state, products) {
    state.products = products;
  }
};
export const actions = {
  loadProducts({ commit }) {
    axios
      .get("http://localhost:3000/products")
      .then(response => {
        commit("LOAD_PRODUCTS", response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
};
export const getters = {
  productLength: state => {
    return state.products.length;
  },
  inStock: state => {
    return state.products.filter(product => product.inventory > 10).length;
  },
  almostSoldOut: state => {
    return state.products.filter(
      product => product.inventory <= 10 && product.inventory > 0
    ).length;
  },
  outOfStock: state => {
    return state.products.filter(product => product.inventory <= 0).length;
  }
};
