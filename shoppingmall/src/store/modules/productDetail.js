export const state = {
  product: {}
};
export const mutations = {
  SHOW_PRODUCTDETAIL(state, product) {
    state.product = product;
  }
};
export const actions = {
  showProductDetail({ commit }, product) {
    commit("SHOW_PRODUCTDETAIL", product);
  }
};
export const getters = {};
